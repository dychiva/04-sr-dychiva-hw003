import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.ArrayList;
import java.util.Scanner;

public class Volunteer extends StaffMember {
    Scanner in = new Scanner(System.in);

    //constructor
    public Volunteer() {
    }

    public Volunteer(String name, String address) {
        super(name, address);
    }

    public void show() {
        System.out.println("Id =" + getId());
        System.out.println("Name = " + getName());
        System.out.println("Address =" + getAddress());
        System.out.println("Thanks you!");
    }

    public String toString() {
        Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,
                ShownBorders.SURROUND_HEADER_AND_COLUMNS);
        t.setColumnWidth(0, 20, 25);
        t.addCell("Volunteer Employee");
        t.addCell("ID = " + getId());
        t.addCell("Name = " + getName());
        t.addCell("Address = " + getAddress());
        t.addCell("Thanks You!");
        return t.render();
    }

    //insert data
    public void Insert(ArrayList<StaffMember> arrList) {
        System.out.print("=> Enter Staff Member's Name :");
        name = in.nextLine();
        System.out.print("=> Enter Staff Member's Address :");
        address = in.nextLine();
        arrList.add(new Volunteer(name, address));
    }

    @Override
    public double pay() {
        return 0;
    }
}
