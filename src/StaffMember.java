import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.Comparator;
import java.util.Scanner;

public abstract class StaffMember {
    protected static int count = 0;
    protected int id;
    protected String name;
    protected String address;
    Scanner in = new Scanner(System.in);

    //Constructor
    public StaffMember() {
    }

    public StaffMember(String name, String address) {
        count++;
        id = count;
        this.name = name;
        this.address = address;
    }

    //setter
    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    //getter
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }


    public void show() {
        System.out.println("Id =" + getId());
        System.out.println("Name = " + getName());
        System.out.println("Address =" + getAddress());
    }

    public String toString() {
        Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,
                ShownBorders.SURROUND_HEADER_AND_COLUMNS);
        t.setColumnWidth(0, 20, 25);
        t.addCell("ID = " + getId());
        t.addCell("Name = " + getName());
        t.addCell("Address = " + getAddress());
        t.addCell("Thanks You!");
        return t.render();
    }

    //abstract method
    public abstract double pay();

    //Compare name method
    public static Comparator<StaffMember> EmpNameComparator = new Comparator<StaffMember>() {
        public int compare(StaffMember s1, StaffMember s2) {
            String StudentName1 = s1.getName().toUpperCase();
            String StudentName2 = s2.getName().toUpperCase();
            return StudentName1.compareTo(StudentName2);
        }
    };

    public void Insert() {
        System.out.print("=> Enter Staff Member's Name :");
        name = in.next();
        System.out.print("=> Enter Staff Member's Address :");
        address = in.next();
    }

//    public boolean equals(Object e) {
//        Integer i = (Integer) e;
//        if (this.getId() == i) return true;
//        return false;
//    }
}
