import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.ArrayList;

public class SalariedEmployee extends StaffMember {
    private double salary;
    private double bonus;

    //setter
    public void setSalary(double salary) {
        this.salary = salary;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    //getter
    public double getSalary() {
        return salary;
    }

    public double getBonus() {
        return bonus;
    }

    //constructors
    public SalariedEmployee() {
    }

    public SalariedEmployee(String name, String address, double salary, double bonus) {
        super(name, address);
        this.salary = salary;
        this.bonus = bonus;
    }

    public String toString() {
        Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,
                ShownBorders.SURROUND_HEADER_AND_COLUMNS);
        t.setColumnWidth(0, 20, 25);
        t.addCell("Salaried Employee");
        t.addCell("ID = " + getId());
        t.addCell("Name = " + getName());
        t.addCell("Address = " + getAddress());
        t.addCell("Salary = " + getSalary());
        t.addCell("Bonus = " + getBonus());
        if (getSalary() < 0 || getBonus() < 0) {
            t.addCell("Payment = null");
        } else {
            t.addCell("Payment = " + pay());
        }
        return t.render();
    }

    public void show() {
        System.out.println("Id =" + getId());
        System.out.println("Name = " + getName());
        System.out.println("Address =" + getAddress());
        System.out.println("Salary = " + getSalary());
        System.out.println("Bonus =" + getBonus());
        System.out.println("Payment = " + pay());
    }

    public void Insert(ArrayList<StaffMember> arrList) {
        System.out.print("=> Enter Staff Member's Name :");
        name = in.nextLine();
        System.out.print("=> Enter Staff Member's Address :");
        address = in.nextLine();
        System.out.print("=> Enter Staff Member's Salary :");
        salary = in.nextDouble();

        System.out.print("=> Enter Staff Member's Bonus :");
        bonus = in.nextDouble();
        if (salary < 0 || bonus < 0) {
            System.out.println("negative number, can't insert plz input again");

        } else {
            System.out.println();
            arrList.add(new SalariedEmployee(name, address, salary, bonus));
        }
    }

    @Override
    public double pay() {
        return getSalary() + getBonus();
    }
}
