import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.util.ArrayList;

public class HourlyEmployee extends StaffMember {
    private int hoursWorked;
    private double rate;

    //setter
    public void setHoursWorked(int hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    //getter
    public int getHoursWorked() {
        return hoursWorked;
    }

    public double getRate() {
        return rate;
    }

    public HourlyEmployee() {
    }

    public HourlyEmployee(String name, String address, int hoursWorked, double rate) {
        super(name, address);
        this.hoursWorked = hoursWorked;
        this.rate = rate;
    }

    public String toString() {
        Table t = new Table(1, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,
                ShownBorders.SURROUND_HEADER_AND_COLUMNS);
        t.setColumnWidth(0, 20, 25);
        t.addCell("Hours Employee");
        t.addCell("ID = " + getId());
        t.addCell("Name = " + getName());
        t.addCell("Address = " + getAddress());
        t.addCell("HoursWorked = " + getHoursWorked());
        t.addCell("Rate = " + getRate());
        if (getHoursWorked() < 0 || getRate() < 0) {
            t.addCell("Payment = null");
        } else {
            t.addCell("Payment = " + pay());
        }
        return t.render();
    }

    public void show() {
        System.out.println("Id =" + getId());
        System.out.println("Name = " + getName());
        System.out.println("Address =" + getAddress());
        System.out.println("Hour = " + getHoursWorked());
        System.out.println("Rate =" + getRate());
        System.out.println("Payment = " + pay());
    }

    public void Insert(ArrayList<StaffMember> arrList) {
        System.out.print("=> Enter Staff Member's Name :");
        name = in.nextLine();
        System.out.print("=> Enter Staff Member's Address :");
        address = in.nextLine();
        System.out.print("=> Enter Staff Member's hoursWorked :");
        hoursWorked = in.nextInt();
        System.out.print("=> Enter Staff Member's rate :");
        rate = in.nextDouble();
        if (hoursWorked < 0 || rate < 0) {
            System.out.println("Negative number, can't insert plz input again");
        } else {
            arrList.add(new SalariedEmployee(name, address, hoursWorked, rate));
        }
    }

    @Override
    public double pay() {
        return getHoursWorked() * getRate();
    }
}
