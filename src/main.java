import com.sun.tools.javac.Main;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.*;

import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;
import org.nocrala.tools.texttablefmt.CellStyle.HorizontalAlign;

public class main {
    public static boolean isNumber;
    public static Scanner in = new Scanner(System.in);
    public ArrayList<StaffMember> arrList = new ArrayList<StaffMember>();
    public static SalariedEmployee salariedEmp = new SalariedEmployee();
    public static HourlyEmployee hourlyEmp = new HourlyEmployee();
    public static Volunteer volunteer = new Volunteer();

    //outer menu
    public static void menu() {
        Table t = new Table(4, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,
                ShownBorders.SURROUND_HEADER_FOOTER_AND_COLUMNS);
        t.addCell("1). Add Employee");
        t.addCell("2). Edit");
        t.addCell("3). Remove");
        t.addCell("4). Exit");
        System.out.println(t.render());
    }

    //inner menu
    public static void OptionAddEmployee() {
        Table t = new Table(4, BorderStyle.UNICODE_BOX_DOUBLE_BORDER_WIDE,
                ShownBorders.SURROUND_HEADER_FOOTER_AND_COLUMNS);
        t.addCell("1). Volunteer");
        t.addCell("2). salaried Employee");
        t.addCell("3). Hourly Employee");
        t.addCell("4). Back");
        System.out.println(t.render());
    }

    //show data
    public static void show(ArrayList<StaffMember> arrList) {
        Collections.sort(arrList, StaffMember.EmpNameComparator);
        for (StaffMember str : arrList) {
            System.out.println(str.toString());
            System.out.println("_________________________________________________");
        }
    }

    //Edit Method
    public void Edit() {
        String nameUp;
        String addUp;
        double salaryUp;
        double bonusUp;
        int hourUp;
        double rateUp;
        int idUpdate = 0;
        boolean exist = false;
        System.out.println("==========Edit Employee==========");
        do {
            System.out.println("=> Enter Employee ID to Update :");
            if (in.hasNextInt()) {
                idUpdate = in.nextInt();
                isNumber = true;
            } else {
                isNumber = false;
                in.next();
                System.out.print("Invalid Input, must be integer only");
            }
        } while (!isNumber);
        for (int i = 0; i < arrList.size(); i++) {
            if (arrList.get(i).getId() != 0 && arrList.get(i).getId() == idUpdate) {
                exist = true;
                arrList.get(i).show();
                System.out.println("=============New Information of staff member===============");
                if (arrList.get(i) instanceof Volunteer) {
                    System.out.print("Enter Staff's member name :");
                    nameUp = in.next();
                    arrList.get(i).setName(nameUp);
                    show(arrList);
                } else if (arrList.get(i) instanceof SalariedEmployee) {
                    System.out.print("Enter Staff's member name :");
                    nameUp = in.next();
                    System.out.print("Enter Staff's member address :");
                    addUp = in.next();
                    System.out.print("Enter Staff's member salary :");
                    salaryUp = in.nextDouble();
                    System.out.print("Enter Staff's member bonus :");
                    bonusUp = in.nextDouble();
                    arrList.get(i).setAddress(addUp);
                    arrList.get(i).setName(nameUp);
                    ((SalariedEmployee) arrList.get(i)).setSalary(salaryUp);
                    ((SalariedEmployee) arrList.get(i)).setBonus(bonusUp);
                    show(arrList);

                } else {
                    System.out.print("Enter Staff's member name :");
                    nameUp = in.next();
                    System.out.print("Enter Staff's member address :");
                    addUp = in.next();
                    System.out.print("Enter Staff's member HourWorked :");
                    hourUp = in.nextInt();
                    System.out.print("Enter Staff's member Rate :");
                    rateUp = in.nextDouble();
                    arrList.get(i).setAddress(addUp);
                    arrList.get(i).setName(nameUp);
                    ((HourlyEmployee) arrList.get(i)).setHoursWorked(hourUp);
                    ((HourlyEmployee) arrList.get(i)).setRate(rateUp);
                    show(arrList);
                }
            }
        }
        if (exist) {
            System.out.println();
        } else {
            System.out.println("Id <" + idUpdate + "> not exit");
        }
    }

    //Remove method
    public void remove() {
        int id = 0;
        boolean exist = false;
        System.out.println("==========Remove Employee=======");
        do {
            System.out.print("Enter Staff ID to Remove :");
            if (in.hasNextInt()) {
                id = in.nextInt();
                isNumber = true;
            } else {
                isNumber = false;
                in.next();
                System.out.print("Invalid Input, must be integer only");
            }
        } while (!isNumber);
        for (int i = 0; i < arrList.size(); i++) {
            if (arrList.get(i).getId() == id) {
                arrList.get(i).show();
                arrList.remove(arrList.get(i));
                System.out.println("Removed sucessfuly");
                System.out.println("====================================================");
                System.out.println("\nEmployee after remove");
                show(arrList);
                exist = true;
            }
        }
        if (!exist) {
            System.out.println("Id <" + id + "> is not exit");
        }
    }

    void init() {

        arrList.add(new Volunteer("chiva", "Battambong"));
        arrList.add(new SalariedEmployee("momo", "PP", 200, 2));
        arrList.add(new HourlyEmployee("NANA", "Kompong Cham", -20, 3));
        Collections.sort(arrList, StaffMember.EmpNameComparator);
        for (StaffMember str : arrList) {
            System.out.println(str.toString());
            System.out.println("______________________________________________");
        }
        int choice;
        int choiceEmp;

        do {
            menu();
            System.out.print("=>choose option(1->4) : ");
            while (!in.hasNextInt()) {
                in.next();
                System.out.println("Invalid Input, must be integer only (1-4)");
                menu();
            }
            choice = in.nextInt();
            System.out.println("\n");
            outer:
            switch (choice) {
                case 1:
                    do {
                        OptionAddEmployee();
                        System.out.print("=>choose option(1->4) : ");
                        while (!in.hasNextInt()) {
                            in.next();
                            System.out.println("Invalid Input, must be integer only (1-4)");
                            menu();
                        }
                        choiceEmp = in.nextInt();
                        switch (choiceEmp) {
                            case 1:
                                System.out.println("==========Insert info Volunteer==========");
                                volunteer.Insert(arrList);
                                show(arrList);
                                break outer;
                            case 2:
                                System.out.println("==========Insert info SalariedEmployee==========");
                                salariedEmp.Insert(arrList);
                                show(arrList);
                                break outer;
                            case 3:
                                System.out.println("==========Insert info HourlyEmployee=======");
                                hourlyEmp.Insert(arrList);
                                show(arrList);
                                break outer;
                            case 4:
                                break outer;

                            default:
                                System.out.println("Choose Invalid Option");
                        }

                    } while (choiceEmp != 4);
                    break;
                case 2:
                    Edit();
                    break;
                case 3:
                    remove();
                    break;
                case 4:
                    int[] surrogates = {0xD83D, 0xDC7D};
                    String alienEmojiString = new String(surrogates, 0, surrogates.length);
                    System.out.println("(^-^) Good Bye!" + alienEmojiString);
                    System.exit(0);
                default:
                    System.out.println("Choose Invalid Option");
            }
        } while (choice != 4);

    }

    public static void main(String[] args) {
        main m = new main();
        m.init();
    }
}


